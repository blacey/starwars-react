# Star Wars React App

Build a React App that does the following:

- Fetches and lists all starships.
- View details about a starship when interacting with it (length, crew size, etc).
- Preserve some starship data in a state holder, such as Redux.

[View Source Code Repo](https://bitbucket.org/blacey/starwars-react/src/master/)

## Improvements
- Modal or popup carousel to display details on each starship
- Add in formatting of the cost value
- Nicer user interface using styled components
- Better user experience with an animated mobile menu and service worker for offline mode and progressive web app usage
- A heart button for each of the ship's that is stored in state and shown on the favourites page
- Use .env for the API URL and API token so it's not added to version control
- Using redux for state management or recoil depending on size and complexity of the project
- Paginated table with dynamic requests using lodash to calculate the pagination and using state to change the page number and refresh the results
- Making use of local storage for the starship data and favourites state, which can then be updated as the data changes or new starships are added
- Ability to select your faction, which sets the website theme to light (Jedi) or dark (Sith) mode
- Using styled components for a page header image
- Update the data table to allow ordering of data based on column click
- I wanted to use TranslateZ and perspective CSS properties on the credits page to imitate the star wars intro where the text disappears towards the top of the page and gets narrower, but this is an enhancement and not a core part of the project

## How to use

```bash
git clone https://bitbucket.org/blacey/starwars-react starwars-react
cd starwars-react
yarn && yarn dev
```
### Notes
- Add in Typescript
- Upgrade to use Redux or Recoil
- Build more components for data display
- Integrate modals for the individual ship details
- Add in unit testing