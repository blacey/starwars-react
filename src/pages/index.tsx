import React, { useState, useEffect } from 'react'
import DataTable from 'components/DataTable'

require('es6-promise').polyfill();
require('isomorphic-fetch');

export default function Home() {
  const [data, setData] = useState([]);
  const [q, setQ] = useState("");

  useEffect( () => {
    fetch("https://swapi.dev/api/starships/?format=json")
    .then(response => response.json())
    .then(json => setData(json["results"]))
  }, []);

  function search(rows){
    return rows.filter( (row) => 
      row.name.toLowerCase().indexOf(q) > -1 || 
      row.model.toLowerCase().indexOf(q) > -1
    );
  }

  return (
    <>
      <section className="body">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <h1>Starships</h1>

              <p>This will show a data table of the starships data held in state with the option to filter their values and a button to favourite / view details in a modal window.</p>

              <div className="filter-form">
                <form>
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <div className="input-group-text">
                          <i className="fa fa-search"></i>
                        </div>
                      </div> 
                      <input id="search" name="search" type="text" placeholder="Search name or model" aria-describedby="searchHelpBlock" className="form-control" value={q} onChange={ (e) => setQ(e.target.value)} />
                    </div> 
                    <span id="searchHelpBlock" className="form-text text-hidden">Search name or model</span>
                  </div>
                </form>
              </div>
              
              <DataTable showColumns={["name", "model", "cost_in_credits", "crew", "passengers", "length"]} data={search(data)} />

            </div>
          </div>
        </div>
      </section>

    </>
  )
}