import styled from 'styled-components'
import Link from 'next/link'
import { useState, useEffect } from 'react'

export default function Favourites() {
  // Get starships that have been added to the favourites state
  const [favourites, setFavourites] = useState([])

  return (
    <>
      <section className="body container">
        <div className="row">
          <div className="col-xs-12">
            <h1>Favourite Starships</h1>
            <p>This will use cards to show what starships have been marked as your favourite with a button to remove favourite.</p>

            <div className="cards">
              {favourites && favourites.map((starship, index) => (
                <>
                  <div key={index} className="card">

                      <img src={`/images/starships/${starship.id}.jpg`} />
                    
                      <h2>{starship.name}</h2>
                      <p>
                        ID: {starship.id}<br/>
                        Model: {starship.model}<br/>
                        Cost: {starship.cost}<br/>
                      </p>
                    </div>  
                </>
              ))}

              {!favourites}
                <p>No Favourites Selected - please <Link href="/">add one</Link></p>
              
            </div>

          </div>
        </div>
      </section>

    </>
  )
}