import styled from 'styled-components'
import Menu from '../components/Menu';

export default function Credits() {
  return (
    <>
      <div className="site-body">
        <div className="credits">
          <div className="wrapper">
            <div className="text-scroll">
              <header>
                <p>A long time ago in a GitHub repository far far away ... </p>
              </header>

              <section className="episode one">
                <h2>Episode I - The Phantom Menace</h2>
                <p>Turmoil has engulfed the Galactic Republic. The taxation of trade routes to outlying star systems is in dispute.</p>
                <p>Hoping to resolve the matter with a blockade of deadly battleships, the greedy Trade Federation has stopped all shipping to the small planet of Naboo.</p>
                <p>While the Congress of the Republic endlessly debates this alarming chain of events, the Supreme Chancellor has secretly dispatched two Jedi Knights, the guardians of peace and justice in the galaxy, to settle the conflict...</p>
              </section>

              <section className="episode two">
                <h2>Episode II - Attack Of The Clones</h2>
                <p>There is unrest in the Galactic Senate. Several thousand solar systems have declared their intentions to leave the Republic.</p>
                <p>This separatist movement, under the leadership of the mysterious Count Dooku, has made it difficult for the limited number of Jedi Knights to maintain peace and order in the galaxy.</p>
                <p>Senator Amidala, the former Queen of Naboo, is returning to the Galactic Senate to vote on the critical issue of creating an ARMY OF THE REPUBLIC to assist the overwhelmed Jedi...</p>
              </section>
            </div>
            </div>
        </div>
      </div>
    </>
  )
}
