
import Head from 'next/head'
import Menu from '../components/Menu'
import 'bootstrap/dist/css/bootstrap.css'
import "../../public/css/global.css";

export default function App({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Star Wars React App</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"></link>
      </Head>
      
      <header className="site-header">  
        <Menu />
      </header>

      <Component {...pageProps} />
    </>
  )
}
