import { array } from "prop-types";
import React, {useState} from "react";

//const [modal, setShowModal] = useState();

export function showModal(){
    return (
        alert('Show the modal!')
    )
}

const setFavourite = (props: any) => {
    alert('Set this ship as your favourite')
    
    let array = favourites;
    let addArray = true;

    array.map((item: any, key:number) => {
        if(item === props.i) {
            array.splice(key, 1);
            addArray = false;
        }
    });

    if(addArray){
        array.push(props.i);
    }
    setFavourite([...array]);

    localStorage.setItem("favourites", JSON.stringify(favourites));
    var storage = localStorage.getItem('favourites' + (props.i) || '0');
    
    if(storage == null){
        localStorage.setItem(('favItem' + (props.i)), JSON.stringify(props.items));
    } else {
        localStorage.removeItem('favItem' + (props.i));
    }
}


export default function DataTable( { showColumns, data }) {
    // Defaults to showing the columns passed into the data table. If a list of columns is not passed in then get the first row of the data and use that
    const columns = showColumns ? showColumns : data[0] && Object.keys(data[0]);

    const [favourites, addFavourite] = useState([] as Array<number>);

    return (
        <div className="responsive">
            <table>
                <thead>
                    <tr>
                        {data[0] && columns.map( (heading, index) => <th key={index}>
                            {
                                heading.replace(new RegExp(`_+`, 'ig'), " ")
                            }
                        </th>)}
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    {data.map( (row, index) => <tr key={index}>
                        {
                            columns.map( column => (
                                <td>
                                    {row[column]}
                                </td>
                            ))
                        }
                        
                        {row["actions"]}
                        <td>
                            <button onClick={ (e) => setFavourite() }>Add Favourite</button>
                            <button onClick={showModal}>View Details</button>
                        </td>
                    </tr>)}
                </tbody>
            </table>
        </div>
    )
}
